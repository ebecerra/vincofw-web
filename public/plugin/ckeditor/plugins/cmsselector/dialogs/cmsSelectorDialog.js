CKEDITOR.dialog.add('cmsSelectorDialog', function (editor) {
    var selectItems = [];
    var tableKeys = [
        {tableKey: 'cms_slideshow', tableId: 'ff80808153eb22ea0153eb2c0c2c00e6', tableName:editor.lang.cmsselector.slideshow}
    ];
    /**
     * Updates the list of elements according to the selected Selector
     *
     * @param dialog CKEditor dialog
     * @param type Selected selector Type
     */
    var setSelectId = function (dialog, type) {
        var injector = editor.angularInjector;
        var adTableService = injector.get("adTableService");
        var cacheService = injector.get("cacheService");
        var autogenService = injector.get("autogenService");
        var $log = injector.get("$log");
        if (type){
            dialog.setValueOf('tab-main', 'selectType', type);
        }
        var cmsSelectorTableId = '';
        for (var i = 0; i < tableKeys.length; i++) {
            if (tableKeys[i].tableKey == dialog.getValueOf('tab-main', 'selectType')) {
                cmsSelectorTableId = tableKeys[i].tableId;
                break;
            }
        }
        adTableService.query({
            idClient: cacheService.loggedIdClient(),
            id: cmsSelectorTableId
        }, function (data) {
            var _service = autogenService.getAutogenService(data);
            _service.query(
                {
                    idClient: cacheService.loggedIdClient()
                }, function (data) {
                    $log.info(data.content);
                    selectItems = data.content;
                    var element = CKEDITOR.dialog.getCurrent().getContentElement("tab-main", "selectId");
                    element.clear();
                    for (var i = 0; i < data.content.length; i++) {
                        element.add(data.content[i].name, data.content[i].id);
                    }
                    if (!CKEDITOR.dialog.getCurrent().insertMode
                        && dialog.getValueOf('tab-main', 'selectType') == CKEDITOR.dialog.getCurrent().updateType
                        && CKEDITOR.dialog.getCurrent().updateId)
                        dialog.setValueOf('tab-main', 'selectId', CKEDITOR.dialog.getCurrent().updateId);

                }, function (error) {
                    $log.error(error);
                });
        }, function (error) {
            $log.error(error);
        });
    };
    var comboItems = [];
    for (var i = 0; i < tableKeys.length; i++) {
        var item = [];
        item.push(tableKeys[i].tableName);
        item.push(tableKeys[i].tableKey);
        comboItems.push(item);
    }

    return {
        title: editor.lang.cmsselector.title,
        minWidth: 500,
        minHeight: 100,
        contents: [
            {
                id: 'tab-main',
                label: editor.lang.cmsselector.title,
                elements: [
                    {
                        type: 'select',
                        id: 'selectType',
                        label: editor.lang.cmsselector.selector,
                        style: "width : 500px;",
                        default: "cms_slideshow",
                        items: comboItems/*[
                            [editor.lang.cmsselector.slideshow, 'cms_slideshow']
                        ]*/,
                        onChange: function () {
                            setSelectId(CKEDITOR.dialog.getCurrent());
                        },
                        validate: CKEDITOR.dialog.validate.notEmpty("Selector cannot be empty.")
                    },
                    {
                        type: 'select',
                        id: 'selectId',
                        label: editor.lang.cmsselector.element,
                        style: "width : 500px;",
                        items: [],
                        validate: CKEDITOR.dialog.validate.notEmpty("Element cannot be empty.")
                    }
                ]
            }
        ],
        onShow: function () {
            var selection = editor.getSelection();
            var element = selection.getStartElement();
            if (element)
                element = element.getAscendant('cms_selector_type', true);
            if (!element || element.getName() != 'cms_selector_type') {
                // element = editor.document.createElement('abbr');
                this.insertMode = true;
            }
            else
                this.insertMode = false;
            this.element = element;
            if (!this.insertMode) {
                this.setupContent(this.element);
                this.updateId =$(element).attr('id');
                this.updateType =$(element).attr('type');
                setSelectId(this, $(element).attr('type'));
            }

        },
        onOk: function () {
            var dialog = this;
            var cmsSelector;
            if (CKEDITOR.dialog.getCurrent().insertMode) {
                cmsSelector = editor.document.createElement('cms_selector_type');
            }
            else{
                cmsSelector = this.element;
            }
            cmsSelector.setAttribute('type', dialog.getValueOf('tab-main', 'selectType'));
            cmsSelector.setAttribute('id', dialog.getValueOf('tab-main', 'selectId'));
            var selectedItem = "";
            for (var i = 0; i < selectItems.length; i++) {
                if (selectItems[i].id == dialog.getValueOf('tab-main', 'selectId')) {
                    selectedItem = selectItems[i].name;
                    break;
                }
            }
            cmsSelector.setHtml('<b>' + selectedItem + '</b>');
            if (CKEDITOR.dialog.getCurrent().insertMode) {
               editor.insertElement(cmsSelector);
            }
            else{
                this.commitContent( cmsSelector );
            }
        }
    };
});