CKEDITOR.plugins.add( 'cmsselector', {
    icons: 'cmsselector',
    lang: "en,es",
    init: function( editor ) {
        CKEDITOR.dialog.add( 'cmsSelectorDialog', this.path + 'dialogs/cmsSelectorDialog.js' );
        editor.ui.addButton( 'CmsSelector', {
            label: editor.lang.cmsselector.title,
            command: 'cms_selector_type',
            toolbar: 'insert'
        });
        editor.addCommand( 'cms_selector_type', new CKEDITOR.dialogCommand( 'cmsSelectorDialog' ), {
            allowedContent: 'cms_selector_type[type,id]',
            requiredContent: 'cms_selector_type',
            contentForms: [
                'cms_selector_type',
                'cms_selector_type'
            ]
        } );
        // Open our dialog on double click
        editor.on( 'doubleclick', function( evt )
        {
            var element = evt.data.element;
            if ( $(element.$).parent('cms_selector_type').length>0)
            {
                evt.data.dialog = 'cmsSelectorDialog';
            }
        }, null, null, 20);
    }
});