﻿/*
 Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.plugins.setLang("cmsselector", "en", {
        title: "Cms Selector",
        selector: "Selector",
        element: "Element",
        slideshow: "Slideshow"
    }
);