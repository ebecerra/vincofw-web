define(['angular',
    'angular-couch-potato',
    'angular-ui-router'], function (ng, couchPotato) {

    "use strict";


    var module = ng.module('app.layout', ['ui.router']);


    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider, $urlRouterProvider) {


        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/layout.tpl.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                'modules/forms/directives/input/smartDatepicker',
                                'modules/forms/directives/validate/smartValidateForm',
                                'layout/controllers/LayoutCtrl',
                                'layout/controllers/resetPasswordController',
                                'layout/directives/customSref',
                                'layout/directives/preventMenuAction',
                                'layout/directives/preventMenuHide'
                            ])
                        }
                    }
                }
            });
        $urlRouterProvider.otherwise('/home');

    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;

});
