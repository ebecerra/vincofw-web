define(['app'], function (module) {

    "use strict";

    module.registerController('HeaderCtrl', function ($scope, Authentication, $location, tabManagerService, cacheService) {
        $scope.logout = function () {
            Authentication.logout(function() {
                tabManagerService.clearTabs();
                cacheService.clearAllFormInfo();
                $location.path('/login');
            });
        };
    });

});
