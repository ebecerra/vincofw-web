define(['app'], function (app) {

    "use strict";

    app.registerController('resetPasswordController', function ($scope, $rootScope, Authentication, adUserService, Base64, $http, commonService) {


        $scope.changePassword = function () {
            $scope.resetPasswordForm.$dirty = true;

            var loginData = Authentication.getLoginData();
            if ($scope.resetPasswordForm.$valid) {
                $('.jarviswidget-loader').show();
                adUserService.changePassword({
                    idClient: Authentication.loggedIdClient(),
                    oldPassword: hex_md5($scope.currentPassword),
                    newPassword: hex_md5($scope.newPassword),
                    idUser: loginData.idUserLogged
                }, function (data) {

                    $('.jarviswidget-loader').hide();
                    var authdata = Base64.encode(loginData.userLogin + ':' + hex_md5($scope.newPassword));
                    loginData.authorization = authdata;
                    loginData.passwd = hex_md5($scope.newPassword);
                    Authentication.setLoginData(loginData);

                    $rootScope.globals = {
                        currentUser: {
                            username: loginData.userLogin,
                            authdata: authdata,
                            fullname: loginData.userName
                        }
                    };
                    $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
                    Authentication.setCookieJSON('globals', $rootScope.globals);
                     $('li.dropdown-tabbed.custom-menu').removeClass('open');
                    $scope.currentPassword = "";
                    $scope.newPassword = "";
                    $scope.confirmPassword = "";
                    commonService.showSuccesNotification($rootScope.getMessage('AD_GlobalPasswordChanged'));
                }, function (data) {
                    $('.jarviswidget-loader').hide();
                    if (data.status == 428)
                        commonService.showRejectNotification($rootScope.getMessage('AD_GlobalPasswordNotChanged'));
                    else
                        commonService.showRejectNotification($rootScope.getMessage('AD_titleError'));
                });
            }
        }
    })
});
