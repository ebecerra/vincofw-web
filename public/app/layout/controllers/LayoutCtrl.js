define(['app'], function (app) {

    "use strict";

    app.registerController('LayoutCtrl', [
        '$scope', '$rootScope', '$stateParams', '$location', '$state', 'Authentication', 'adMenuService', '$window', '$http', 'tabManagerService', 'cacheService', '$log', 'globalConfigService',
        function ($scope, $rootScope, $stateParams, $location, $state, Authentication, adMenuService, $window, $http, tabManagerService, cacheService, $log, globalConfigService) {

            $scope.getTabs = function () {
                return tabManagerService.tabs;
            };

            $scope.footerTemplateUrl = globalConfigService.footerTemplateUrl;
            $scope.clientFullName = globalConfigService.clientFullName;
            $scope.logoClientPath = globalConfigService.logoClientPath;

            /**
             * Goes the the state defined by the tab
             *
             * @param tab Tab to analize
             */
            $scope.go = function (tab) {
                tabManagerService.go(tab);
            };

            /**
             * Deletes a tab from the list
             *
             * @param index Tab index
             */
            $rootScope.removeTab = function (index) {
                tabManagerService.removeTab(index);
            };

            $rootScope.$on('GLOBAL_GET_WORD_CREATED', tabManagerService.changeTab);

            $rootScope.$on("$stateChangeSuccess", tabManagerService.changeTab);

            this.logout = function () {
                Authentication.logout();
                $location.path('/login');
            };
            $rootScope.menus = [];

            /**
             * Loads the menu
             */
            $scope.loadMenu = function () {
                var params = {
                    idClient: cacheService.loggedIdClient(),
                    'q': 'username=' + Authentication.getLoginData().userLogin,
                    idLanguage: Authentication.getCurrentLanguage()
                };
                if (localStorage["selected_role"] !== undefined) {
                    params.roleId = localStorage["selected_role"];
                }
                if (Authentication.isUserLogged() && cacheService.loggedIdClient()) {
                    adMenuService.query(
                        params,
                        function (data) {
                            $rootScope.menus = data.content;
                            var hasRegistered = false;
                            $rootScope.updateMenu = false;
                            $scope.$watch(function () {
                                if (hasRegistered) {
                                    if (!$rootScope.updateMenu) {
                                        $rootScope.updateMenu = true;
                                    }
                                    return;
                                }
                                hasRegistered = true
                            });
                        },
                        function () {
                            // Error
                        }
                    );
                    $scope.userName = Authentication.getLoginData().userName;
                }
            };
            if (Authentication.isUserLogged() && cacheService.loggedIdClient()) {
                $scope.loadMenu();
            }

            $scope.$on(app.eventDefinitions.GLOBAL_USER_LOGGED, function () {
                $scope.loadMenu();
            });

            $scope.visualizeProcess = function (data) {
                tabManagerService.analyzeProcess(data);
            };

            /**
             * Returns the correct manual command to insert into the angular data-ui-sref tag
             * @param menu Menu to analyze
             * @returns {*} Angular state with parenthesis included
             */
            $scope.getManualCommand = function(menu){
                if (menu.manualCommand){
                    var hasParenthesis = /^.+\(.*\)$/g;
                    var command = menu.manualCommand;
                    var params = '()';
                    if (menu.value && menu.value.length > 0) {
                        params = "({value : '" + menu.value + "'})";
                    }
                    return hasParenthesis.test(command) ? menu.manualCommand : menu.manualCommand + params;
                }
                else {
                    return '.';
                }
            };

            cacheService.loadLanguages();
        }]);
});
