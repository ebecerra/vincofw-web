define(['layout/module', 'jquery'], function (module) {

    "use strict";

    (function ($) {

        $.fn.smartCollapseToggle = function () {

            return this.each(function () {

                var $body = $('body');
                var $this = $(this);

                // only if not  'menu-on-top'
                if ($body.hasClass('menu-on-top')) {


                } else {

                    $body.hasClass('mobile-view-activated')

                    // toggle open
                    $this.toggleClass('open');

                    // for minified menu collapse only second level
                    if ($body.hasClass('minified')) {
                        if ($this.closest('nav ul ul').length) {
                            // $this.find('>a .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                            $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                        }
                    } else {
                        // toggle expand item
                        // $this.find('>a .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                        $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                    }
                }
            });
        };
    })(jQuery);

    module.registerDirective('smartMenu', function ($state, $rootScope, $timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var $body = $('body');

                var render = function(){
                    var $collapsible = element.find('li[data-menu-collapse]');
                    $collapsible.each(function (idx, li) {
                        var $li = $(li);
                        $li.off('click');
                        $li
                            .on('click', function (e) {
                                // collapse all open siblings
                                $li.siblings('.open').smartCollapseToggle();

                                // toggle element
                                $li.smartCollapseToggle();

                                render();
                                e.preventDefault();
                            });

                        // initialization toggle
                        if ($li.find('li.active').length) {
                            $li.smartCollapseToggle();
                        }

                    });
                    // click on route link
                    element.on('click', 'a[data-ui-sref]', function (e) {
                        // collapse all siblings to element parents and remove active markers
                        $(this)
                            .each(function () {
                                $(this).siblings('li.open').smartCollapseToggle();
                                $(this).siblings('li').removeClass('active')
                            });

                        if ($body.hasClass('mobile-view-activated')) {
                            $rootScope.$broadcast('requestToggleMenu');
                        }
                    });
                    scope.$on('$smartLayoutMenuOnTop', function (event, menuOnTop) {
                        if (menuOnTop) {
                            $collapsible.filter('.open').smartCollapseToggle();
                        }
                    });
                };
                $rootScope.$watch('updateMenu', function (val) {
                    if (val){
                        render();
                    }
                });

                $timeout(render, 30000)
            }
        }
    });


});
