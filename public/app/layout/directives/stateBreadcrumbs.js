define(['layout/module'], function (module) {

    'use strict';

    module.registerDirective('stateBreadcrumbs', function ($rootScope, $state, $stateParams, $compile, $timeout) {


        return {
            restrict: 'E',
            replace: true,
            template: '<ol class="breadcrumb"><li>Home</li></ol>',

            compile: function (tElement, tAttributes) {


                return {
                    post: function (scope, element, attrs, ngModel) {

                        function setBreadcrumbs(breadcrumbs, paramNames) {
                            var html = '<li>' + $rootScope.getWord('app.global.home') + '</li>';
                            angular.forEach(breadcrumbs, function (crumb, idx) {
                                if (idx + 1 != breadcrumbs.length) {
                                    var refObject = "{";
                                    _.each(paramNames, function (current) {
                                        refObject += (current + ":\"" + crumb.params[current] + "\",")
                                    });
                                    refObject += "}";
                                    var stateData = crumb.name + "(" + refObject + ")";
                                    var link = "<a style='color: #E4E4E4;' class='breadcrumb-path' data-ui-sref='" + stateData + "'>";
                                    html += ('<li>' + link + $rootScope.getWord(crumb.title) + "</a>" + '</li>');
                                }
                                else {
                                    html += ('<li>' + $rootScope.getWord(crumb.title) + '</li>');
                                }
                            });
                            var titleSplitted = document.title.split(" | ");
                            if (titleSplitted.length > 1) {
                                document.title = $rootScope.getWord(titleSplitted[0]);
                                document.title += " | ";
                                document.title += (" "+$rootScope.getWord(titleSplitted[1]));
                            }
                            element.html($compile(angular.element(html))(scope));
                        }

                        function fetchBreadcrumbs(stateName, breadcrunbs) {

                            var state = $state.get(stateName);

                            if (state && state.data && state.data.title && breadcrunbs.indexOf(state.data.title) == -1) {
                                var realStateName = $state.get(stateName).abstract ? (stateName + ".table") : stateName;
                                if ($state.get(stateName).father) {
                                    realStateName = $state.get(stateName).father;
                                }
                                breadcrunbs.unshift({
                                    title: state.data.title,
                                    params: $state.params,
                                    name: realStateName
                                })
                            }

                            var parentName = stateName.replace(/.?\w+$/, '');
                            if (parentName) {
                                return fetchBreadcrumbs(parentName, breadcrunbs);
                            } else {
                                return breadcrunbs;
                            }
                        }

                        function processState(state) {
                            var breadcrumbs;
                            if (state.data && state.data.breadcrumbs) {
                                breadcrumbs = state.data.breadcrumbs;
                            } else {
                                breadcrumbs = fetchBreadcrumbs(state.name, []);
                            }
                            var paramsSplited = $state.current.url.match("[:][^/]+");
                            var paramNames = _.keys($state.params);
                            $timeout(function () {
                                setBreadcrumbs(breadcrumbs, paramNames);
                            }, 0, false);

                        }

                        $rootScope.$on('GLOBAL_GET_WORD_CREATED', function () {
                            processState($state.current);
                        });
                        //processState($state.current);

                        $rootScope.$on('$stateChangeStart', function (event, state) {
                            processState(state);
                        })
                    }
                }
            }
        }
    });
});
