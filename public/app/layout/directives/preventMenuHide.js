define(['layout/module'], function (module) {

    'use strict';

    module.registerDirective('preventMenuHide', function ($window, Authentication, $rootScope, adClientService, adUserService, $log) {
        return {
            restrict: 'A',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        $rootScope.updateProfile = false;
                        scope.updateProfileCheck = function (data) {
                            $rootScope.updateProfile = data;
                        };
                        element.on('click', function (event) {
                            $(this).parent().toggleClass('open');
                        });
                        $('body').on('click', function (e) {
                            if (!$('li.dropdown-tabbed.custom-menu').is(e.target)
                                && $('li.dropdown-tabbed.custom-menu').has(e.target).length === 0
                                && $('.open').has(e.target).length === 0
                            ) {
                                $('li.dropdown-tabbed.custom-menu').removeClass('open');
                            }
                        });

                        if (Authentication.isUserLogged()) {
                            var user = Authentication.getUser();
                            scope.loggedClient = adClientService.get({idClient: Authentication.loggedIdClient()});

                            scope.reloadInformation = function () {
                                var idClient = 0;
                                _.each(Authentication.getUser().roles, function(current){
                                    if ($rootScope.checkedRole && current.idRole ==$rootScope.checkedRole.idRole)
                                        idClient = current.idClient;
                                });
                                adUserService.get({
                                    q: 'idUser=' + user.idUser,
                                    idClient: idClient
                                }, function(data) {
                                    if (data.content.length > 0) {
                                        var user = data.content[0];
                                        if ($rootScope.updateProfile) {
                                            user.defaultIdLanguage = $rootScope.selectedLanguage.id;
                                            user.defaultIdRole = $rootScope.checkedRole.id;
                                        }
                                        Authentication.updateUser(user, function () {
                                            $window.location.reload();
                                        }, function (error) {
                                            $log.error(error);
                                        });
                                    }
                                    else{
                                        $window.location.reload();
                                    }
                                }, function(error){
                                    $log.error(error);
                                })

                            }
                        }
                    }
                }
            }
        }
    });
});
