define(['layout/module'], function (module) {

    'use strict';

    module.registerDirective('customUiSref', function ($state, $rootScope, $timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var link = element.attr("custom-ui-sref");
                element.removeAttr("custom-ui-sref");
                if (link=="#"){
                    element.removeAttr("data-ui-sref");
                    element.attr("href","#");
                }
            }
        }
    });
});

