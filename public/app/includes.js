define([

    // account
    'auth/module',
    'auth/models/User',
    'auth/services/Authentication',
    'auth/directives/autoFillSync',

    // configuration
    'configuration/module',
    'configuration/service/globalConfigService',

    // layout
    'layout/module',
    'layout/actions/minifyMenu',
    'layout/actions/toggleMenu',
    'layout/actions/fullScreen',
    'layout/actions/resetWidgets',
    'layout/directives/smartInclude',
    'layout/directives/smartDeviceDetect',
    'layout/directives/smartFastClick',
    'layout/directives/smartLayout',
    'layout/directives/smartSpeech',
    'layout/directives/smartRouterAnimationWrap',
    'layout/directives/smartFitAppView',
    'layout/directives/dismisser',
    'layout/directives/smartMenu',
    'layout/directives/bigBreadcrumbs',
    'layout/directives/stateBreadcrumbs',
    'layout/directives/smartPageTitle',
    'layout/directives/hrefVoid',
    'layout/directives/preventMenuAction',
    'layout/directives/preventMenuHide',
    'layout/service/SmartCss',
    'layout/controllers/LayoutCtrl',
    'layout/controllers/HeaderCtrl',
    

    'modules/forms/directives/validate/smartValidateForm',
    'modules/widgets/directives/jarvisWidget',


    // dashboard
    'dashboard/module'

], function () {
    'use strict';
});
