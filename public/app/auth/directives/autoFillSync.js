define(['auth/module'], function (module) {

    "use strict";

    return module.registerDirective('autoFillSync', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attrs) {
                var origVal = elem.val();
                elem.removeAttr('auto-fill-sync');
                $timeout(function () {
                    var newVal = elem.val();
                    if (this && ngModel.$pristine && origVal !== newVal) {
                        ngModel.$setViewValue(newVal);
                    }
                }, 0);
            }
        };
    }]);
});