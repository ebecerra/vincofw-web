define(['auth/module'], function (module) {

    "use strict";

    return module.registerDirective('globalSignin', function ($rootScope) {
        return {
            restrict: 'E',
            template: '<a class="btn btn-primary btn-block m-b">{{getMessage("AD_GlobalLogin")}}</a>',
            replace: true,
            link: function (scope, element) {
                element.on('click', function(){
                        $rootScope.$broadcast('event:global-signin-success');
                });
            }
        };
    });
});
