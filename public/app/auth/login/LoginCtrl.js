define(['auth/module'], function (module) {

    "use strict";

    module.registerController('LoginCtrl', [
        '$scope', '$rootScope', '$location', '$state', 'Authentication',
        'Language', 'User', 'commonService', '$timeout', 'cacheService', 'processSocketService', 'globalConfigService',
        function ($scope, $rootScope, $location, $state, Authentication,
                  Language, User, commonService, $timeout, cacheService, processSocketService, globalConfigService) {

            $scope.redirectCounter = 10;
            $scope.redirectMessage = "";
            $scope.clientFullName = globalConfigService.clientFullName;
            $scope.logoClientPath = globalConfigService.logoClientPath;
            $scope.loginWelcomeUrl = globalConfigService.loginWelcomeUrl;
            cacheService.getMessages();

            if ($location.search()) {
                $scope.date = $location.search().date;
                $scope.hash = $location.search().hash;
                $scope.userId = $location.search().userId;
            }
            $scope.getMessage = $rootScope.getMessage;

            $scope.doLogin = function () {
                $scope.loginForm.$setDirty();
                if ($scope.loginForm.$valid) {
                    Authentication.deleteCookie(COOKIE_USER);
                    Authentication.deleteCookie(COOKIE_USER_PRIVILEGES);
                    Authentication.login($scope.username, hex_md5($scope.password), function (response) {
                        $rootScope.languages = [];
                        cacheService.loadLanguages();
                        if ($scope.remember) {
                            Authentication.rememberCredentials($scope.username, hex_md5($scope.password), response.userName, response.idUser);
                        } else {
                            Authentication.rememberCredentials("", "", "", "");
                        }
                        User.username = response.userName;
                        $location.path('/home');

                        processSocketService.initialize(null, null);
                    }, function (response, code) {
                        if (code == 403) {
                            commonService.showError($rootScope.getMessage('AD_GlobalUserNotAuth'));
                        } else {
                            commonService.showError($rootScope.getMessage('AD_ErrValidationUnknow'));
                        }
                    });
                }
            };

            $scope.$on('event:global-signin-success', function (event) {
                $scope.doLogin();
            });

            $scope.resetPassword = function () {
                if ($scope.confirmPassword != $scope.password)
                    commonService.showError($rootScope.getMessage('AD_GlobalPasswordNotMatch'));
                else if ($scope.resetForm.$valid) {
                    Authentication.resetPassword($scope.date, $scope.hash, $scope.userId, hex_md5($scope.password), function () {
                        $location.path('/resetSuccess');
                        $rootScope.redirectCounter = 10;
                        var incCounter = function () {
                            if ($scope.redirectCounter > 0) {
                                $scope.redirectCounter--;
                                $scope.redirectMessage = $rootScope.getMessage('AD_GlobalRedirectAuto', [$rootScope.redirectCounter]);
                                $timeout(incCounter, 1000);
                            } else {
                                $location.path('/home');
                            }
                        };
                        incCounter();
                    }, function (response, code) {
                        commonService.showError($rootScope.getMessage(response.message));
                    });
                }
            };

            $scope.recoverPassword = function () {
                $scope.forgotForm.validated = true;
                if ($scope.forgotForm.$valid) {
                    Authentication.recoverPassword($scope.email, function (response) {
                        $state.go('login');
                        commonService.showHint($rootScope.getMessage('AD_GlobalMailPasswordSent'));
                    }, function (response, code) {
                        commonService.showError($rootScope.getMessage(response.message));
                    });
                }
            }
        }]);
});
