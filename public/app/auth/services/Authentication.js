define(['app'], function (app) {

    'use strict';

    return app.registerFactory('Authentication',
        function (Base64, $http, $cookies, $rootScope, $log, commonService, adUserService, adRoleService, adPreferenceService) {
            var service = {};

            service.roles = [];
            service.permissions = [];

            service.COOKIE_USER_CLIENT = "user_client";

            /**
             * Loads logged user's permissions
             */
            service.loadPermissions = function () {
                var selected = _.find(service.roles, function (current) {
                    return current.idRole == localStorage["selected_role"];
                });
                if (!selected && service.roles.length > 0) {
                    selected = service.roles[0];
                    localStorage["selected_role"] = service.roles[0].idRole;
                }
                if (selected) {
                    var idClient = selected.idClient;
                    adPreferenceService.permissions({
                        idClient: idClient,
                        idRole: localStorage["selected_role"]
                    }, function (data) {
                        service.permissions = data.properties.permissions;
                        $rootScope.$broadcast(app.eventDefinitions.GLOBAL_USER_LOGGED);
                    }, function (err) {
                        $log.error(err);
                    });
                }
            };

            /**
             * Loads logged user's role information
             */
            service.loadRoleInfo = function () {
                if (service.roles.length > 0)
                    return;
                var loginData = service.getLoginData();
                if (loginData) {
                    service.roles = loginData.roles;
                    service.loadPermissions();
                }
            };

            /**
             * Logs the user in
             *
             * @param username      Username
             * @param password      Password
             * @param callbackOk    OK callback function
             * @param callbackFail  Fail callback function
             */
            service.login = function (username, password, callbackOk, callbackFail) {
                var that = this;
                that.setCredentials(username, password);
                /* Use this for real authentication
                 ----------------------------------------------*/
                $http.get(REST_HOST + 'login/privileges?login=' + username, {/* username: username, password: password */})
                    .success(function (response) {
                        if (response.properties.isUser) {
                            localStorage.removeItem(appName + "_includeRole");
                            var loginData = response.properties;
                            localStorage["selected_role"] =loginData.defaultIdRole ? loginData.defaultIdRole : loginData.user.defaultIdRole;
                            localStorage["selected_language"] = loginData.user.defaultIdLanguage;
                            var authdata = Base64.encode(username + ':' + password);
                            loginData.authorization = authdata;
                            loginData.passwd = password;
                            loginData.userLogin = username;
                            that.setLoginData(loginData);
                            that.setCredentials(username, password, loginData.userName);
                            if (callbackOk)
                                callbackOk(response.properties);
                        } else {
                            commonService.showError($rootScope.getMessage('AD_GlobalUserNotAuth'));
                            that.clearCredentials();
                            if (callbackFail)
                                callbackFail(response);
                        }
                    })
                    .error(function (response, code) {
                        $log.error(response);
                        that.clearCredentials();
                        if (callbackFail)
                            callbackFail(response, code);
                    });
            };

            /**
             * Logs the user out
             *
             * @param callbackOk   OK callback function
             * @param callbackFail Fail callback function
             */
            service.logout = function (callbackOk, callbackFail) {
                var that = this;
                if (this.isUserLogged()) {
                    $http.get(REST_HOST_CORE + 'login/logout', {})
                        .success(function (response) {
                            that.clearCredentials();
                            service.roles = [];
                            service.preferences = [];
                            $rootScope.selectedLanguage = null;
                            localStorage[appName + "_login"] = "";
                            localStorage[appName + "_pwd"] = "";
                            localStorage['selected_language'] = "";
                            if (callbackOk)
                                callbackOk(response);
                        })
                        .error(function (response) {
                                console.log(response);
                                that.clearCredentials();
                                if (callbackOk)
                                    callbackOk();
                            }
                        );
                } else {
                    this.clearCredentials();
                    callbackOk && callbackOk();
                }
            };

            /**
             * Stores login information in cookie.
             *
             * @param user Login information
             */
            service.setLoginData = function (user) {
                if (user) {
                    if (user.privileges) {
                        var mapped = _.map(user.privileges, function (current) {
                            return {
                                idPrivilege: current.id,
                                name: current.name
                            }
                        });
                        this.setCookieJSON(COOKIE_USER_PRIVILEGES, mapped);
                        user.privileges = undefined;
                    }
                    if (user.user) {
                        if (user.user.client) {
                            var client = {
                                idClient: user.user.client.idClient,
                                name: user.user.client.name
                            };
                            this.setCookieJSON(service.COOKIE_USER_CLIENT, client);
                            user.user.client = undefined;
                        }
                        user.idUser = user.user.idUser;
                        user.defaultIdRole = user.user.defaultIdRole;
                        if (!user.userName)
                            user.userName = user.user.name;
                    }
                    user.user = undefined;

                    this.setCookieJSON(COOKIE_USER, user);
                }
            };

            /**
             * Logs out and changes the user default role
             *
             * @param user User information
             * @param callbackOk OK callback function
             * @param callbackFail Fail callback function
             */
            service.updateUser = function (user, callbackOk, callbackFail) {
                var that = this;
                adUserService.update(user, function (response) {
                    localStorage[appName + "_includeRole"] = true;
                    var loginData = that.getLoginData();
                    var authData = Base64.encode($rootScope.checkedRole.idRole + '$' + loginData.userLogin + ':' + loginData.passwd);
                    loginData.user = user;
                    loginData.authorization = authData;
                    that.setLoginData(loginData);
                    if (callbackOk)
                        callbackOk(response);
                }, function (response) {
                    if (callbackFail)
                        callbackFail(response);
                });
            };

            /**
             * Checks if the user is loged
             *
             * @returns {boolean}
             */
            service.isUserLogged = function () {
                var user = this.getUser();
                return !(typeof(user) == 'undefined' || user == null || user == "null");
            };

            /**
             * Checks if the user is registered.
             *
             * @returns {*}
             */
            service.getUser = function () {
                return this.getLoginData();
            };

            /**
             * Remembers credentials in local storage
             *
             * @param username Username
             * @param password Password
             * @param fullname Fullname
             */
            service.rememberCredentials = function (username, password, fullname, idClient) {
                localStorage[appName + "_login"] = username;
                localStorage[appName + "_pwd"] = password;
                localStorage[appName + "_fullName"] = fullname;
                localStorage[appName + "_idClient"] = idClient;
            };

            /**
             * Recovers the password
             *
             * @param email     Email
             * @param success   OK Callback function
             * @param fail      Fail Callback function
             */
            service.recoverPassword = function (email, success, fail) {
                $http.get(REST_HOST_CORE + 'ad_user/0/recover?email=' + email, {})
                    .success(function (response) {
                        if (response.message == null) {
                            if (success)
                                success();
                        } else {
                            if (fail)
                                fail(response, response.errCode);
                        }
                    })
                    .error(function (response) {
                        console.log(response);
                        if (fail)
                            fail(response, response.errCode);
                    });
            };

            /**
             * Resets the password
             *
             * @param date      Date
             * @param hash      Hash
             * @param userId    UserId
             * @param password  New password
             * @param success   OK callback function
             * @param fail      Fail callback function
             */
            service.resetPassword = function (date, hash, userId, password, success, fail) {
                $http.get(REST_HOST_CORE + 'ad_user/0/reset', {
                    params: {
                        userId: userId,
                        date: date,
                        hash: hash,
                        password: password
                    }
                })
                    .success(function (response) {
                        if (response.message == null) {
                            service.login(response.properties.login, password, success, fail);
                        } else {
                            if (fail)
                                fail(response, response.code);
                        }
                    })
                    .error(function (response) {
                        console.log(response);
                        if (fail)
                            fail(response, response.code);
                    });
            };

            /**
             * Returns the stored credentials
             *
             * @return {null}
             */
            service.getRememberCredentials = function () {
                if (localStorage[appName + "_login"] != "" && localStorage[appName + "_login"] !== undefined) {
                    return {
                        'Username': localStorage[appName + "_login"],
                        'Password': localStorage[appName + "_pwd"],
                        'Fullname': localStorage[appName + "_fullName"],
                        'IdClient': localStorage[appName + "_idClient"]
                    }
                }
                return null;
            };

            /**
             * Gets the active role for the registered user
             *
             * @returns {*}
             */
            service.getCurrentRole = function () {
                return localStorage["selected_role"];
            };


            service.loggedIdClient = function () {
                var selected = _.find(service.roles, function (current) {
                    return current.idRole == localStorage["selected_role"];
                });
                if (selected) {
                    return selected.idClient;
                }
                return null;
            };

            /**
             * Gets the active language for the registered user
             *
             * @returns {*}
             */
            service.getCurrentLanguage = function () {
                return localStorage["selected_language"];
            };

            /**
             * Stores the credentials in a cookie
             *
             * @param username User name
             * @param password Password
             * @param fullname Full name
             */
            service.setCredentials = function (username, password, fullname) {
                var authdata = Base64.encode(username + ':' + password);
                if (localStorage[appName + "_includeRole"])
                    authdata = Base64.encode(localStorage['selected_role'] + '$' + username + ':' + password);

                $rootScope.globals = {
                    currentUser: {
                        username: username,
                        authdata: authdata,
                        fullname: fullname
                    }
                };
                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
                this.setCookieJSON('globals', $rootScope.globals);
            };

            /**
             * Refresh credentials stored in the cookie
             */
            service.resetGlobals = function () {
                var user = this.getLoginData();
                this.setCredentials(user.userLogin, user.passwd, user.userName);
            };


            /**
             * Stores a cookie.
             *
             * @param c_name  Cookie name
             * @param c_value Cookie value
             */
            service.saveCookie = function (c_name, c_value) {
                this.setCookie(c_name, c_value);
            };

            /**
             * De;etes a cookie.
             *
             * @param cookieName Cookie name
             */
            service.deleteCookie = function (cookieName) {
                $cookies.remove(cookieName);
            };

            /**
             * Clears credentials
             */
            service.clearCredentials = function () {
                $rootScope.globals = {};
                $cookies.remove('globals');
                $cookies.remove(COOKIE_USER);
                $cookies.remove(COOKIE_USER_PRIVILEGES);
                localStorage['selected_role'] = "";
                $http.defaults.headers.common.Authorization = 'Basic ';
                localStorage[appName + "_login"] = "";
                localStorage[appName + "_pwd"] = "";
                localStorage.removeItem(appName + "_includeRole")
                // localStorage[appName + "_includeRole"] = undefined;
                localStorage[appName + "_fullName"] = "";
                localStorage[appName + "_idClient"] = "";
                $http.defaults.headers.common['Authorization'] = null;
            };

            /**
             * Gets login information stored in cookies
             * @returns {*}
             */
            service.getLoginData = function () {
                var loginData = this.getCookieJSON(COOKIE_USER);
                var privileges = this.getCookieJSON(COOKIE_USER_PRIVILEGES);
                if (loginData) {
                    loginData.privileges = privileges;
                }
                return loginData;
            };

            /**
             * Stores a JSON object in a cookie
             */
            service.setCookieJSON = function (c_name, c_value) {
                var data = JSON.stringify(c_value);
                this.setCookie(c_name, data);
            };

            /**
             * Returns the content of a cookie
             *
             * @param c_name Cookie name
             * @returns      Cookie content
             */
            service.loadCookie = function (c_name) {
                return this.getCookie(c_name);
            };

            /**
             * Stores a cookie.
             *
             * @param c_name  Cookie name
             * @param c_value Cookie value
             */
            service.setCookie = function (c_name, c_value) {
                var exdate = new Date();
                var timeout = new Date(exdate.getTime() + (SESSION_TIMEOUT));
                $cookies.put(c_name, escape(c_value), {expires: timeout});
            };

            /**
             * Gets a JSON object stored in a cookie.
             * @param c_name Cookie name
             * @returns {*}  JSON stored
             */
            service.getCookieJSON = function (c_name) {
                var result = this.getCookie(c_name);

                if (result && result != 'undefined' && typeof(result) != 'undefined' && result != null && result != "null") {
                    result = JSON.parse(result);
                    return result;
                }
                return undefined;
            };
            service.getCookie = function (c_name) {
                return unescape($cookies.get(c_name));
            };

            /**
             * Checks if the logged user has one of the given privileges
             *
             * @param privileges Privilege or array of privileges
             * @returns {boolean} True if the user contains any of the given privileges
             */
            $rootScope.hasPrivilege = function (privileges) {
                var privs = [];
                privs = privs.concat(privileges);
                var originalPrivs = _.map(service.getLoginData().privileges, function (current) {
                    return current.name;
                });
                var intersection = _.intersection(privs, originalPrivs);
                return intersection.length == privs.length;
            };

            service.hasPrivilege = $rootScope.hasPrivilege;

            /**
             * Checks if the role matches the active role
             *
             * @param role Role to check
             * @returns {boolean} True if the passed role matches the active role
             */
            $rootScope.validRole = function (role) {
                return _.some(service.roles, function (current) {
                    return current.name == role && current.idRole == localStorage["selected_role"];
                });
            };

            /**
             * Checks if the role matches the active role
             *
             * @param role Role to check
             * @returns {boolean} True if the passed role matches the active role
             */
            $rootScope.hasRole = function (role) {
                return _.some(service.roles, function (current) {
                    return current.name == role;
                });
            };

            /**
             * Checks if the role matches the active role
             *
             * @param property Property to check
             * @returns {boolean} True if user has the permission and is allowd
             */
            $rootScope.hasPermission = function (property) {
                return _.some(service.permissions, function (current) {
                    return current.property == property && current.allow;
                });
            };

            return service;
        }).registerFactory('Base64', function () {
        /* jshint ignore:start */

        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

        return {
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                do {
                    enc1 = keyStr.indexOf(input.charAt(i++));
                    enc2 = keyStr.indexOf(input.charAt(i++));
                    enc3 = keyStr.indexOf(input.charAt(i++));
                    enc4 = keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };

        /* jshint ignore:end */
    });

});
