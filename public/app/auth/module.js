define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-google-plus',
    'angular-cookies',
    'angular-easyfb'
], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.auth', [
        'ui.router',
        'ngCookies'
    ]);

    couchPotato.configureApp(module);

    var authKeys = {
        googleClientId: '678402726462-ah1p6ug0klf9jm8cplefmphfupg3bg2h.apps.googleusercontent.com',
        facebookAppId: '620275558085318'
    };

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider.state('realLogin', {
            url: '/real-login',

            views: {
                root: {
                    templateUrl: "app/auth/views/login.html",
                    controller: 'LoginCtrl',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'auth/models/User',
                            'auth/login/LoginCtrl'
                        ])
                    }
                }
            },
            data: {
                title: 'Login',
                rootId: 'extra-page'
            }

        })

        .state('login', {
            url: '/login',
            views: {
                root: {
                    controller: 'LoginCtrl',
                    templateUrl: 'app/auth/views/login.html'
                }
            },
            data: {
                title: 'Login',
                htmlId: 'extr-page'
            },
            resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                    'auth/services/Authentication',
                    'auth/login/LoginCtrl',
                    'modules/forms/directives/validate/smartValidateForm',
                    'auth/directives/globalSignin'
                ])
            }
        })

        .state('register', {
            url: '/register',
            views: {
                root: {
                    templateUrl: 'app/auth/views/register.html'
                }
            },
            data: {
                title: 'Register',
                htmlId: 'extr-page'
            },
            resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                    'modules/forms/directives/validate/smartValidateForm'
                ])
            }
        })

        .state('resetPassword', {
            url: '/resetPassword',
            views: {
                root: {
                    controller: 'LoginCtrl',
                    templateUrl: 'app/auth/views/resetPassword.html'
                }
            },
            data: {
                title: 'Recrear contraseña'
            },
            resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                    'auth/services/Authentication',
                    'auth/login/LoginCtrl',
                    'modules/forms/directives/validate/smartValidateForm'
                ])
            }
        })

        .state('resetSuccess', {
            url: '/resetSuccess',
            views: {
                root: {
                    controller: 'LoginCtrl',
                    templateUrl: 'app/auth/views/resetSuccess.html'
                }
            },
            data: {
                title: 'Contraseña modificada'
            },
            resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                    'auth/login/LoginCtrl',
                    'modules/forms/directives/validate/smartValidateForm'
                ])
            }
        })

        .state('forgotPassword', {
            url: '/forgot-password',
            views: {
                root: {
                    controller: 'LoginCtrl',
                    templateUrl: 'app/auth/views/forgot-password.html'
                }
            },
            data: {
                title: 'Forgot Password',
                htmlId: 'extr-page'
            },
            resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                    'modules/forms/directives/validate/smartValidateForm',
                    'auth/services/Authentication',
                    'auth/login/LoginCtrl'
                ])
            }
        })

        .state('lock', {
            url: '/lock',
            views: {
                root: {
                    templateUrl: 'app/auth/views/lock.html'
                }
            },
            data: {
                title: 'Locked Screen',
                htmlId: 'lock-page'
            }
        })


    }).constant('authKeys', authKeys);

    module.run(function($couchPotato){
        module.lazy = $couchPotato;
    });
    return module;
});