define(['auth/module'], function (module) {

    'use strict';

   return module.registerFactory('User', function ($http, $q, $rootScope) {
        var dfd = $q.defer();

        var UserModel = {
            initialized: dfd.promise,
            username: undefined,
            picture: undefined
        };

       // keep user logged in after page refresh
       var user = $rootScope.globals.currentUser;

        return UserModel;
    });

});
