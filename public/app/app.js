'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-animate',
    'angular-cookies',
    'angular-bootstrap',
    'angular-sanitize',
    'smartwidgets',
    'chartjs',
    'angular-easy-pie',
    'notification'
], function (ng, couchPotato) {

    var app = ng.module('app', [
        'ngSanitize',

        'scs.couch-potato',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'easypiechart',
        // App
        'app.auth',
        'app.layout',
        'app.dashboard',
        'app.globalConfiguration',

        // Aplication modules
        'app.configuration',
        'app.autogen',
        'app.process'
    ]);


    // Application's common angular  events
    app.eventDefinitions = {
        TRANSLATIONS_READY: "EVENTS_TRANSLATIONS_READY",            // Triggered when all the translations have been loaded
        SUB_ITEM_EDIT: "EVENTS_SUB_ITEM_EDIT",                      // Triggered when an item of a tab of level >= 1 is being edited or created
        CANCEL_SUB_ITEM_EDIT: "EVENTS_CANCEL_SUB_ITEM_EDIT",        // Triggered when the form of level >= 1 is being closed (saved or canceled)
        CANCEL_GLOBAL_ITEM_EDIT: "EVENTS_CANCEL_GLOBAL_ITEM_EDIT",  // Triggered when the form of level 0 is being closed (saved or canceled)

        GLOBAL_ITEM_EDIT: "EVENTS_GLOBAL_ITEM_EDIT",                // Triggered when an item of a tab of level 0 is being edited or created
        GLOBAL_ITEM_CREATED: "EVENTS_GLOBAL_ITEM_CREATED",          // Triggered when the item related to the form has been loaded (to load the form's related tabs)
        GLOBAL_PROCESS_FINISHED: "EVENTS_GLOBAL_PROCESS_FINISHED",  // Triggered when a process has finished
        GLOBAL_USER_LOGGED: "EVENTS_GLOBAL_USER_LOGGED",            // Triggered when a user has logged in successfully
        GLOBAL_TAB_CLOSED: "EVENTS_GLOBAL_TAB_CLOSED",              // Triggered when a tab is closed
        GLOBAL_TAB_SELECTED: "EVENTS_GLOBAL_TAB_SELECTED",          // Triggered when a tab is closed

        TABLE_RESPONSIVE_LOADED: "EVENTS_TABLE_RESPONSIVE_LOADED",  // Triggered when a tab is closed
        TAB_VISUALIZATION_NOTIFICATION: "EVENTS_TAB_VISUALIZATION_NOTIFICATION",// Triggered when a field is changed in a form tab, to notify sibling tabs

        CHART_ITEM_CLICKED: "EVENTS_CHART_ITEM_CLICKED",            // Triggered when a chart item is clicked
        CHART_RESET_SEARCH: "EVENTS_CHART_RESET_SEARCH",            // Triggered when a reset filter parameter is requested
        CHART_UPDATE_FILTER_DESC: "CHART_UPDATE_FILTER_DESC"        // Triggered when a chart is filtered by clicking in linked chart
    };

    couchPotato.configureApp(app);

    app.config(function ($provide, $httpProvider) {
        $provide.factory('JarvisLoaderHttpInterceptor', function ($q, $log) {
            var requestCounter = 0;

            return{
                request: function(config) {
                    requestCounter++;
                    // $log.debug("Starting request " + requestCounter);
                    var jarvisLoader = $('.jarviswidget-loader');
                    if (requestCounter > 0 && jarvisLoader)
                        jarvisLoader.show();
                    return config || $q.when(config);
                },
                requestError: function(rejection) {
                    var jarvisLoader = $('.jarviswidget-loader');
                    requestCounter--;
                    // $log.debug("Remaining requests " + requestCounter);
                    if (requestCounter === 0 && jarvisLoader)
                        jarvisLoader.hide();
                    return $q.reject(rejection);
                },
                response: function(response) {
                    var jarvisLoader = $('.jarviswidget-loader');
                    requestCounter--;
                    // $log.debug("Remaining requests " + requestCounter);
                    if (requestCounter === 0 && jarvisLoader)
                        jarvisLoader.hide();
                    // $log.debug("Remaining requests " + requestCounter);
                    return response || $q.when(response);
                },
                responseError: function(rejection) {
                    var jarvisLoader = $('.jarviswidget-loader');
                    requestCounter--;
                    // $log.debug("Remaining requests " + requestCounter);
                    if (requestCounter === 0 && jarvisLoader)
                        jarvisLoader.hide();
                    // $log.debug("Remaining requests " + requestCounter);
                    return $q.reject(rejection);
                }
            }

        });
        $httpProvider.interceptors.push('JarvisLoaderHttpInterceptor');


        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q, $log) {
            var errorCounter = 0;

            function notifyError(rejection) {
                console.log(rejection);
                $.bigBox({
                    title: rejection.status + ' ' + rejection.statusText,
                    content: rejection.data,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
            }

            function showErrorNotification(message, rejection) {
                $log.error(rejection);
                $.smallBox({
                    title: "Fallo",
                    content: message,
                    color: "#C46A69",
                    timeout: 4000,
                    icon: "fa fa-warning"
                });
            }

            function notifyErrorMessage(title, message) {
                $.bigBox({
                    title: title,
                    content: message,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
            }

            return {
                // On request failure
                requestError: function (rejection) {
                    // show notification
                    notifyError(rejection);

                    // Return the promise rejection.
                    return $q.reject(rejection);
                },

                // On response failure
                responseError: function (rejection) {
                    if (rejection.data) {
                        return $q.reject(rejection);
                    } else {
                        // show notification
                        if (rejection.message == "Unexpected token F"
                            || rejection.status == 412
                            || rejection.status == 428) {
                        } else if (rejection.data == "An error has occurred: {\"code\":\"ECONNREFUSED\",\"errno\":\"ECONNREFUSED\",\"syscall\":\"connect\"}"
                            && rejection.status == 500) {
                            showErrorNotification("No hay conexión", rejection);
                        } else if (rejection.status == 403) {
                            notifyErrorMessage("Denegado", "No tiene permisos");
                        } else {
                            notifyErrorMessage("Error en conexión");
                        }
                        // Return the promise rejection.
                        return $q.reject(rejection);
                    }
                }
            };
        });

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');
    });

    app.run(function ($couchPotato, $rootScope, $state, $stateParams, $location, $cookieStore,
                      $http, Authentication, cacheService, tabManagerService, $templateCache) {
        app.lazy = $couchPotato;
        sessionStorage.clear();

        $rootScope.hideTablet = "hidden-md hidden-sm hidden-xs";
        $rootScope.hidePhone = "hidden-sm hidden-xs";

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        // editableOptions.theme = 'bs3';

        $rootScope.previousState = null;
        $rootScope.currentState = null;
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (typeof (current) !== 'undefined') {
                // $templateCache.removeAll();
                $templateCache.remove(current.templateUrl);
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            var redirectPage = $.inArray($location.path(), ['/resetPassword', '/resetSuccess']) === -1;
            if (redirectPage && from.url=='^'){
                $location.path('/home');
            }
            var $body = $('body');
            var $window = $(window);
            if ($window.width() < 979) {
                $body.toggleClass('no-menu', $window.width() < 979);
                $rootScope.showMenu = false;
            }
            if (from.name != to.name) {
                $rootScope.previousState = from.name;
                $rootScope.currentState = to.name;
                console.log('Previous state:' + $rootScope.previousState);
                console.log('Current state:' + $rootScope.currentState);
            }
        });

        // keep user logged in after page refresh
        if (!Authentication.isUserLogged()) {
            if (Authentication.getRememberCredentials() == null)
                Authentication.deleteCookie('globals');
        } else {
            Authentication.resetGlobals();
            Authentication.setCookieJSON(COOKIE_USER, Authentication.getCookieJSON(COOKIE_USER));
            Authentication.setCookieJSON(COOKIE_USER_PRIVILEGES, Authentication.getCookieJSON(COOKIE_USER_PRIVILEGES));
        }
        $rootScope.globals = Authentication.getCookieJSON('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        // Se modifica para hacer un touch de la cookie y que comience de nuevo su periodo de validez
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/resetSuccess', '/login', '/register', '/forgotPassword', '/resetPassword']) === -1;
            if (restrictedPage) {
                if (!Authentication.isUserLogged()) {

                    var storedCredentials = Authentication.getRememberCredentials();
                    if (storedCredentials) {
                        Authentication.setCredentials(storedCredentials.Username, storedCredentials.Password, storedCredentials.Fullname);
                        Authentication.login(
                            storedCredentials.Username, storedCredentials.Password,
                            function (response) {

                            },
                            function (response) {
                                cacheService.clearAllFormInfo();
                                tabManagerService.clearTabs();
                                $location.path('/login');
                            }
                        );
                    } else {
                        if (restrictedPage) {
                            cacheService.clearAllFormInfo();
                            tabManagerService.clearTabs();
                            $location.path('/login');
                        } else {
                            $location.path($location.path());
                        }
                    }
                } else {
                    // Se refresca la cookie
                    Authentication.resetGlobals();
                    Authentication.setCookieJSON(COOKIE_USER, Authentication.getCookieJSON(COOKIE_USER));
                    Authentication.setCookieJSON(COOKIE_USER_PRIVILEGES, Authentication.getCookieJSON(COOKIE_USER_PRIVILEGES));
                    Authentication.loadRoleInfo();
                }
            }
        });
    });
    return app;
});
