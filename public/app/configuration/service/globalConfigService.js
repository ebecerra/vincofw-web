define(['configuration/module'], function (module, _) {

    'use strict';

    module.registerFactory('globalConfigService', function ($rootScope) {

        var service = {};
        service.clientFullName = "";
        service.logoClientPath = "logo_client.png";
        service.footerTemplateUrl = "app/layout/partials/footer.tpl.html";
        service.loginWelcomeUrl = "app/auth/views/loginWelcome.html";
        service.fullApplicationName = "";
        service.applicationIconUrl = "favicon.ico";
        return service;

    });
});



