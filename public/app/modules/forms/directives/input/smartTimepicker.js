define(['app', 'bootstrap-timepicker'], function (app) {

    'use strict';

    return app.registerDirective('smartTimepicker', function () {
        return {
            restrict: 'A',
            compile: function (tElement, tAttributes) {
                tElement.removeAttr('smart-timepicker data-smart-timepicker');
                tElement.timepicker();
            }
        }
    });
});
