/**
 * @ngdoc directive
 * @name app.directive:smartDatepicker
 * @restrict A
 * @description
 * This directive handles the creation of the Datepicker component
 *
 * @param {String} delayed When the attribute is present, the initial date will be set after the angular digest cycle
 * @param {String} dataLeaveBlank When the attribute is present, the initial date will be left in blank if no model is present, otherwise the value of the model will be used
 * @param {String} minRestrict The ID of the component to use as minimum date restriction (in case we decide to use a range date formed by two Datepickers)
 * @param {String} maxRestrict The ID of the component to use as maximum date restriction (in case we decide to use a range date formed by two Datepickers)
 * @param {String} preventKeyboard When the attribute is present, the user will not be able to input dates using the keyboard
 * @param {String} dataMomentDateFormat The format used for moment js date conversion
 * @param {String} dateFormat Date format to use for display
 * @param {Int} minDate Minimum date to use. A number that will be converted to a Date
 * @param {Int} maxDate Minimum date to use. A number that will be converted to a Date
 */
define(['app', 'lodash', 'jquery', 'jquery-ui'], function (app, _, $) {
    "use strict";

    return app.registerDirective('smartDatepicker', function ($timeout, $rootScope) {
        return {
            restrict: 'A',
            require: '?ngModel',
            transclude: true,
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        element.removeAttr('smartDatepicker');
                        var clearText = {
                            'es': 'Limpiar',
                            'en': 'Clear',
                            'fr': 'Clean',
                            'it': 'Clean',
                            'cn': 'Clean'
                        };

                        $.datepicker.regional.es = {
                            closeText: "Cerrar",
                            prevText: "&#x3C;Ant",
                            nextText: "Sig&#x3E;",
                            currentText: "Hoy",
                            monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio",
                                "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
                            monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun",
                                "jul", "ago", "sep", "oct", "nov", "dic"],
                            dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
                            dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
                            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
                            weekHeader: "Sm",
                            dateFormat: "dd/mm/yy",
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: ""
                        };
                        $.datepicker.regional.it = {
                            closeText: "Chiudi",
                            prevText: "&#x3C;Prec",
                            nextText: "Succ&#x3E;",
                            currentText: "Oggi",
                            monthNames: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno",
                                "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
                            monthNamesShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu",
                                "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
                            dayNames: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"],
                            dayNamesShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
                            dayNamesMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"],
                            weekHeader: "Sm",
                            dateFormat: "dd/mm/yy",
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: ""
                        };
                        $.datepicker.regional.ca = {
                            closeText: "Tanca",
                            prevText: "Anterior",
                            nextText: "Següent",
                            currentText: "Avui",
                            monthNames: ["gener", "febrer", "març", "abril", "maig", "juny",
                                "juliol", "agost", "setembre", "octubre", "novembre", "desembre"],
                            monthNamesShort: ["gen", "feb", "març", "abr", "maig", "juny",
                                "jul", "ag", "set", "oct", "nov", "des"],
                            dayNames: ["diumenge", "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte"],
                            dayNamesShort: ["dg", "dl", "dt", "dc", "dj", "dv", "ds"],
                            dayNamesMin: ["dg", "dl", "dt", "dc", "dj", "dv", "ds"],
                            weekHeader: "Set",
                            dateFormat: "dd/mm/yy",
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: ""
                        };
                        $.datepicker.regional.fr = {
                            closeText: "Fermer",
                            prevText: "Précédent",
                            nextText: "Suivant",
                            currentText: "Aujourd'hui",
                            monthNames: ["janvier", "février", "mars", "avril", "mai", "juin",
                                "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
                            monthNamesShort: ["janv.", "févr.", "mars", "avr.", "mai", "juin",
                                "juil.", "août", "sept.", "oct.", "nov.", "déc."],
                            dayNames: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                            dayNamesShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
                            dayNamesMin: ["D", "L", "M", "M", "J", "V", "S"],
                            weekHeader: "Sem.",
                            dateFormat: "dd/mm/yy",
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: ""
                        };

                        var languageIso = 'en';
                        if ($rootScope.selectedLanguage) {
                            languageIso = $rootScope.selectedLanguage.iso2;
                        }
                        $.datepicker.setDefaults(
                            $.extend(
                                $.datepicker.regional[languageIso]
                            )
                        );

                        var onSelectCallbacks = [];
                        if (attrs.minRestrict) {
                            onSelectCallbacks.push(function (selectedDate) {
                                $(attrs.minRestrict).datepicker('option', 'minDate', selectedDate);
                            });
                        }

                        if (attrs.maxRestrict) {
                            onSelectCallbacks.push(function (selectedDate) {
                                $(attrs.maxRestrict).datepicker('option', 'maxDate', selectedDate);
                            });
                        }

                        var options = {
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>',
                            changeYear: true,
                            onSelect: function (selectedDate) {
                                angular.forEach(onSelectCallbacks, function (callback) {
                                    callback.call(this, selectedDate)
                                })
                            },
                            beforeShow: function (input) {
                                setTimeout(function () {
                                    $(input).datepicker("widget").find("[data-handler='today']").remove();
                                    var clearButton = $(input)
                                        .datepicker("widget")
                                        .find(".ui-datepicker-close");
                                    clearButton.unbind("click").bind("click", function () {
                                        $.datepicker._clearDate(input);
                                    });
                                }, 1);
                            }
                        };

                        if (attrs.preventKeyboard) {
                            $(element).on("focus", function () {
                                this.blur();
                            });
                            options.showButtonPanel = true;
                            options.closeText= clearText[languageIso];
                        } else {
                            var validate = function validate(){
                                var format = element.attr("data-moment-date-format");
                                if (typeof(ngModel.$viewValue) === "string"){
                                    var valid = (moment(ngModel.$viewValue, format, true).isValid());
                                    if (valid){
                                        element.datepicker("setDate", moment(ngModel.$viewValue, format).toDate() || '');
                                        $timeout(function(){
                                            ngModel.$setViewValue(moment(ngModel.$viewValue, format));
                                        })
                                    }
                                    return valid;
                                } else if (typeof(ngModel.$viewValue) === typeof(new Date())) {
                                    return true;
                                } return (moment(ngModel.$viewValue).isValid());
                            };

                            var validation = function myValidation(value) {
                                var result = validate();
                                ngModel.$setValidity('dateFormat', result);
                                return value;
                            };
                            ngModel.$parsers.push(validation);
                        }

                        // Se pone la fecha minima
                        if (attrs.minDate) {
                            options.minDate = new Date(parseInt(attrs.minDate));
                            console.log("attrs.dateFormat", attrs.dateFormat);
                            console.log("attrs.minDate", attrs.minDate);
                            console.log("options.minDate", options.minDate);
                        }
                        if (attrs.maxDate) {
                            options.maxDate = new Date(parseInt(attrs.maxDate));
                            console.log("attrs.maxDate", attrs.maxDate);
                            console.log("options.maxDate", options.maxDate);
                        }

                        if (attrs.dateFormat) options.dateFormat = attrs.dateFormat;

                        if (attrs.delayed !== undefined) {
                            element.removeAttr('delayed');
                            $timeout(function () {
                                element.datepicker(options);
                                if (ngModel) {
                                    var format = element.attr("data-moment-date-format");
                                    var validDate = moment(ngModel.$viewValue).isValid();
                                    if (validDate && ngModel.$viewValue && ngModel.$viewValue && ngModel.$viewValue !== "" && !(typeof ngModel.$viewValue === 'string')) {
                                        element.datepicker("setDate", moment(ngModel.$viewValue).toDate());
                                    } else if (format && ngModel.$viewValue && ngModel.$viewValue !== "") {
                                        if (moment(ngModel.$viewValue, format).isValid())
                                            element.datepicker("setDate", moment(ngModel.$viewValue, format).toDate() || '');
                                        else
                                            element.datepicker("setDate", moment(ngModel.$viewValue).toDate() || '');
                                    }
                                    else {
                                        if (element.attr("data-leave-blank") !== undefined) {
                                            element.removeAttr('data-leave-blank');
                                            element.datepicker("setDate", '');
                                        }
                                        else if (ngModel.$viewValue) {
                                            element.datepicker("setDate", new Date(ngModel.$viewValue));
                                        }
                                        else
                                            element.datepicker("setDate", '');
                                    }
                                    ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.

                                        var format = element.attr("data-moment-date-format");
                                        if (format) {
                                            element.datepicker("setDate", moment(ngModel.$viewValue, format).toDate() || '');
                                        }
                                        else {
                                            element.datepicker("setDate", new Date(ngModel.$viewValue) || '');
                                        }
                                    };

                                    onSelectCallbacks = [];
                                    if (tAttributes.minRestrict) {
                                        onSelectCallbacks.push(function (selectedDate) {
                                            $(tAttributes.minRestrict).datepicker('option', 'minDate', selectedDate);
                                        });
                                    }
                                    if (tAttributes.maxRestrict) {
                                        onSelectCallbacks.push(function (selectedDate) {
                                            $(tAttributes.maxRestrict).datepicker('option', 'maxDate', selectedDate);
                                        });
                                    }
                                    onSelectCallbacks.push(function (selectedDate) {
                                        scope.$apply(function () {
                                            var format = element.attr("data-moment-date-format");
                                            if (selectedDate === null || selectedDate === "" || selectedDate === undefined)
                                                ngModel.$setViewValue(null);
                                            else if (format)
                                                ngModel.$setViewValue(moment(selectedDate, format));
                                            // ngModel.$setViewValue(selectedDate);//This will update the model property bound to your ng-model whenever the datepicker's date changes.
                                            scope.$emit('event:datetime-picker-datechanged', selectedDate, element[0].id);

                                        });

                                    });
                                    element.datepicker("option", "onSelect", function (selectedDate) {
                                        angular.forEach(onSelectCallbacks, function (callback) {
                                            callback.call(this, selectedDate)
                                        })
                                    });
                                    scope.$emit('DATE_PICKER_MODEL_VIEW', element[0].id);

                                }
                            })
                        }
                        else
                            element.datepicker(options);
                        if (ngModel) {
                            ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.

                                var format = element.attr("data-moment-date-format");
                                var validDate = moment(ngModel.$viewValue).isValid();
                                if (validDate && ngModel.$viewValue && ngModel.$viewValue && ngModel.$viewValue != "" && !(typeof ngModel.$viewValue === 'string')) {
                                    element.datepicker("setDate", moment(ngModel.$viewValue).toDate());
                                } else if (format && ngModel.$viewValue && ngModel.$viewValue != "") {
                                    if (moment(ngModel.$viewValue, format).isValid())
                                        element.datepicker("setDate", moment(ngModel.$viewValue, format).toDate() || '');
                                    else
                                        element.datepicker("setDate", moment(ngModel.$viewValue).toDate() || '');
                                }
                                else {
                                    if (ngModel.$viewValue && ngModel.$viewValue != "") {
                                        if (element.attr("data-leave-blank") != undefined) {
                                            element.removeAttr('data-leave-blank');
                                            element.datepicker("setDate", new Date(ngModel.$viewValue) || '');
                                        }
                                        else {
                                            element.datepicker("setDate", new Date(ngModel.$viewValue));
                                        }
                                    }

                                    // if (ngModel.$viewValue)
                                    //     element.datepicker("setDate", new Date(ngModel.$viewValue) || '');
                                    else
                                        element.datepicker("setDate", '');
                                }
                            };

                            onSelectCallbacks = [];
                            if (tAttributes.minRestrict) {
                                onSelectCallbacks.push(function (selectedDate) {
                                    $(tAttributes.minRestrict).datepicker('option', 'minDate', selectedDate);
                                });
                            }
                            if (tAttributes.maxRestrict) {
                                onSelectCallbacks.push(function (selectedDate) {
                                    $(tAttributes.maxRestrict).datepicker('option', 'maxDate', selectedDate);
                                });
                            }
                            onSelectCallbacks.push(function (selectedDate) {
                                scope.$apply(function () {
                                    var format = element.attr("data-moment-date-format");
                                    if (selectedDate === null || selectedDate === "" || selectedDate === undefined)
                                        ngModel.$setViewValue(null);
                                    else if (format)
                                        ngModel.$setViewValue(moment(selectedDate, format));
                                    else
                                        ngModel.$setViewValue(selectedDate);//This will update the model property bound to your ng-model whenever the datepicker's date changes.
                                    scope.$emit('event:datetime-picker-datechanged', selectedDate, element[0].id);

                                });

                            });
                            element.datepicker("option", "onSelect", function (selectedDate) {
                                angular.forEach(onSelectCallbacks, function (callback) {
                                    callback.call(this, selectedDate)
                                })
                            });
                        }
                    }
                }
            }
        }
    })
});