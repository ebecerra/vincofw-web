define(['app', 'clockpicker'], function (app) {

    'use strict';

    return app.registerDirective('smartClockpicker', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, element, attrs, ngModel) {
                        element.removeAttr('smart-clockpicker data-smart-clockpicker');

                        var options = {
                            placement: 'bottom'
                        };

                        if (ngModel && ngModel.$viewValue) {
                            options['default'] = ngModel.$viewValue;
                        }
                        else {
                            element.val(moment(new Date()).format("HH:mm"));
                        }
                        ngModel.$render = function () { //This will update the view with your model in case your model is changed by another code.
                            element.val(ngModel.$viewValue);
                            element.clockpicker('remove');
                            options['default'] = ngModel.$viewValue;
                            element.clockpicker(options);
                        };
                        element.on('keypress',
                            function (event) {
                                event.stopPropagation();
                                return false;
                            }
                        );
                        element.on('change',
                            function (event) {
                                ngModel.$setViewValue(element.val());
                            }
                        );
                        element.clockpicker(options);
                    }

                }
            }
        }
    });
});
