define(['app', 'ckeditor'], function (app) {

    'use strict';

    app.registerDirective('smartCkEditor', function ($rootScope, $timeout, Authentication, $log, $injector, $compile) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                idAttr: '=',
                idLanguageAttr: '=?',
                idTableAttr: '=',
                extraTemplates: '=?'
            },
            compile: function (tElement, tAttributes) {
                return {
                    post: function (scope, elm, attr, ngModel) {
                        var initFunction = function () {
                            elm.removeAttr('smart-ck-editor data-smart-ck-editor');
                            var config = {
                                allowedContent: true,
                                toolbar: [
                                    {
                                        name: 'document',
                                        groups: ['mode', 'document', 'doctools'],
                                        items: ['Source', '-', 'Preview', 'Print', '-', 'Templates']
                                    },
                                    {
                                        name: 'clipboard',
                                        groups: ['clipboard', 'undo'],
                                        items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                                    },
                                    {
                                        name: 'editing',
                                        groups: ['find', 'selection', 'spellchecker'],
                                        items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                                    },
                                    {
                                        name: 'insert',
                                        items: ['Image', 'ImageMaps', 'Youtube', 'CmsSelector', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak']
                                    },
                                    '/',
                                    {
                                        name: 'basicstyles',
                                        groups: ['basicstyles', 'cleanup'],
                                        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                                    },
                                    {
                                        name: 'paragraph',
                                        groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                                    },
                                    '/',
                                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                                    {name: 'colors', items: ['TextColor', 'BGColor']},
                                    {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                                    {name: 'others', items: ['-']},
                                    {name: 'links', items: ['Link', 'Unlink']},
                                    {name: 'about', items: ['About']}
                                ],
                                templates: 'extras,default',
                                extraPlugins: 'youtube,imagemaps,uploadimage,cmsselector',
                                language: scope.$root.selectedLanguage.countrycode,
                                on: {
                                    instanceReady: function (argument) {
                                        scope.ckEditorLoaded = true;
                                        scope.addTemplates();
                                    }
                                }
                            };
                            config.baseFloatZIndex = 9000;

                            scope.ckEditorLoaded = false;

                            scope.$watch('extraTemplates', function () {
                                if (scope.ckEditorLoaded) {
                                    scope.addTemplates();
                                }
                            });
                            scope.addTemplates = function () {
                                var templates = [
                                    {
                                        title: "Text and Table",
                                        image: "template3.gif",
                                        description: "A title with some text and a table.",
                                        html: '\x3cdiv style\x3d"width: 80%"\x3e\x3ch3\x3eTitle goes here\x3c/h3\x3e\x3ctable style\x3d"width:150px;float: right" cellspacing\x3d"0" cellpadding\x3d"0" border\x3d"1"\x3e\x3ccaption style\x3d"border:solid 1px black"\x3e\x3cstrong\x3eTable title\x3c/strong\x3e\x3c/caption\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3c/table\x3e\x3cp\x3eType the text here\x3c/p\x3e\x3c/div\x3e'
                                    }
                                ];
                                if (scope.extraTemplates && scope.extraTemplates.length > 0) {
                                    var extraTemplates = _.map(scope.extraTemplates, function (current) {
                                        return {
                                            title: current.title,
                                            image: 'template1.gif',
                                            description: current.description,
                                            html: current.html
                                        };
                                    });
                                    templates = templates.concat(extraTemplates);
                                }
                                CKEDITOR.addTemplates('extras', {
                                    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
                                    templates: templates
                                });
                            };

                            if (CKEDITOR.instances[elm.attr('id')])
                                CKEDITOR.instances[elm.attr('id')].destroy();
                            var getCkEditor = function () {
                                var editor = CKEDITOR.replace(elm.attr('id'), config);
                                editor.angularInjector = $injector;
                                editor.angularCompile = $compile;
                                // The following code is needed to ensure that the editor popup dialogs work when the editor is within a modal dialog
                                // See https://stackoverflow.com/questions/22637455/how-to-use-ckeditor-in-a-bootstrap-modal/23667151#23667151
                                $.fn.modal.Constructor.prototype.enforceFocus = function () {
                                    var $modalElement = this.$element;
                                    $(document).on('focusin.modal', function (e) {
                                        var $parent = $(e.target.parentNode);
                                        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
                                            // add whatever conditions you need here:
                                            &&
                                            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
                                            $modalElement.focus()
                                        }
                                    })
                                };
                                return editor;
                            };
                            var setListeners = function () {
                                ck.on('instanceReady', function () {
                                    ck.setData(ngModel.$viewValue);
                                });
                                ck.on('fileUploadRequest', function (evt) {
                                    $log.info(evt);
                                });
                                ck.on('pasteState', function (evt) {
                                    $log.info(evt);
                                    $timeout(function () {
                                        scope.$apply(function () {
                                            ngModel.$setViewValue(ck.getData());
                                        });
                                    }, 0);

                                });
                                ngModel.$render = function (value) {
                                    ck.setData(ngModel.$viewValue);
                                };
                            };
                            var ck = getCkEditor();
                            setListeners();
                            if (!ngModel) return;

                            scope.$watch('idAttr', function () {
                                if (scope.idAttr) {
                                    var idField = scope.idAttr;
                                    if (scope.idLanguageAttr) {
                                        idField += scope.idLanguageAttr;
                                    }
                                    config.filebrowserImageUploadUrl = REST_HOST_CORE + 'ckeditor/' + Authentication.loggedIdClient() + "/upload/" + scope.idTableAttr + "/" + idField + '?baseUrl=' + REST_HOST_CORE + 'ckeditor/' + Authentication.loggedIdClient() + '/encode/' + scope.idTableAttr + "/" + idField + "/";
                                    config.imageUploadUrl = REST_HOST_CORE + 'ckeditor/' + Authentication.loggedIdClient() + "/imageupload/" + scope.idTableAttr + "/" + idField + '?baseUrl=' + REST_HOST_CORE + 'ckeditor/' + Authentication.loggedIdClient() + '/encode/' + scope.idTableAttr + "/" + idField + "/";
                                    if (typeof(CKEDITOR.instances[elm.attr('id')]) == 'undefined') {
                                        ck = getCkEditor();
                                        setListeners();
                                    } else {
                                        if (CKEDITOR.status == 'unloaded') {
                                            CKEDITOR.on('loaded', function (evt) {
                                                CKEDITOR.instances[elm.attr('id')].removeAllListeners();
                                                CKEDITOR.instances[elm.attr('id')].destroy(true);
                                                ck = getCkEditor();
                                                setListeners();
                                            });
                                        } else {
                                            CKEDITOR.on('instanceDestroyed', function (evt) {
                                                $('.' + evt.editor.id).remove();
                                            });
                                            CKEDITOR.instances[elm.attr('id')].removeAllListeners();
                                            try {
                                                CKEDITOR.instances[elm.attr('id')].destroy(true);
                                            }
                                            catch (e) {
                                                $('.' + CKEDITOR.instances[elm.attr('id')].id).remove();
                                            }
                                            delete CKEDITOR.instances[elm.attr('id')];
                                            ck = getCkEditor();
                                            setListeners();
                                        }
                                    }
                                }
                            });

                        };

                        if (attr.delayed != undefined) {
                            $timeout(initFunction, 0);
                        }
                        else
                            initFunction();
                    }
                }
            }
        }
    });
});
