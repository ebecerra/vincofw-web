/**
 * Jarvis Widget Directive
 *
 *    colorbutton="false"
 *    editbutton="false"
      togglebutton="false"
       deletebutton="false"
        fullscreenbutton="false"
        custombutton="false"
        collapsed="true"
          sortable="false"
 *
 *
 */
define(['../../../app'], function(app){

    "use strict";

    app.registerDirective('jarvisWidget', function($rootScope){
        return {
            restrict: "A",
            compile: function (element, attributes) {
                if (element.data('widget-color'))
                    element.addClass('jarviswidget-color-' + element.data('widget-color'));


                var waitColor = "darken";
                if (element.data('widget-color') === "darken")
                    waitColor = "white";
                element.find('.widget-body').prepend('<div class="jarviswidget-editbox"><input class="form-control" type="text"></div>');

                element.prepend('<span class="jarviswidget-loader ' + 'jarviswidget-color-' + waitColor + '" style="display: none; color: white; position: relative; top: 0px; padding-right: 50px;"><i class="fa fa-refresh fa-spin"></i></span>');
                element.addClass('jarviswidget jarviswidget-sortable');
                $rootScope.$emit('jarvisWidgetAdded', element);


            }
        }
    });
    // app.registerFactory('myHttpInterceptor', function ($q, $log) {
    //     return function (promise) {
    //         $log.info("Starting....");
    //         return promise.then(function (response) {
    //             // do something on success
    //             // todo hide the spinner
    //             //alert('stop spinner');
    //             $log.info("Success");
    //             return response;
    //
    //         }, function (response) {
    //             // do something on error
    //             // todo hide the spinner
    //             //alert('stop spinner');
    //             $log.info("Error");
    //             return $q.reject(response);
    //         });
    //     };
    // });
});